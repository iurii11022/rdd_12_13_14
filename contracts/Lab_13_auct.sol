// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

contract lab_13_auct {
    // Власник контракту
    address public owner;

    // тривалість аукціону та комісія
    uint constant DURATION = 2 days;
    uint constant FEE = 10;

    // Структура аукціону
    struct Auction {
        address payable seller; // Продавець
        uint startingPrice; // Початкова ціна
        uint finalPrice; // Кінцева ціна
        uint startAt; // Час початку
        uint endsAt; // Час закінчення
        uint discountRate; // Розмір знижки
        string item; // Предмет аукціону
        bool stopped; // Стан аукціону (зупинений чи активний)
    }

    // Масив всіх аукціонів
    Auction[] public auctions;

    // Події: створення аукціону і закінчення аукціону
    event AuctionCreated(uint index, string itemName, uint startingPrice, uint duration);
    event AuctionEnded(uint index, uint finalPrice, address winner);

    //встановлення власника контракту при його деплої в блокчейн
    constructor() {
        owner = msg.sender;
    }

    //створення новий аукціон
    function createAuction(uint _startingPrice, uint _discountRate, string memory _item, uint _duration) external {
        uint duration = _duration == 0 ? DURATION : _duration;

        // Перевірка коректності початкової ціни
        require(_startingPrice >= _discountRate * duration, "incorrect starting price");

        // Створення нового аукціону
        Auction memory newAuction = Auction({
        seller: payable(msg.sender),
        startingPrice: _startingPrice,
        finalPrice: _startingPrice,
        discountRate: _discountRate,
        startAt: block.timestamp, // now
        endsAt: block.timestamp + duration,
        item: _item,
        stopped: false
        });

        // Додавання нового аукціону в масив
        auctions.push(newAuction);

        // Виклик події створення аукціону
        emit AuctionCreated(auctions.length - 1, _item, _startingPrice, duration);
    }

    // Функція визначення поточної ціни аукціону
    function getPriceFor(uint index) public view returns(uint) {
        Auction memory cAuction = auctions[index];

        // Перевірка чи данний аукціон не зупинений
        require(!cAuction.stopped, "stopped!");

        // Визначення знижки в залежності від пройденного часу
        uint elapsed = block.timestamp - cAuction.startAt;
        uint discount = cAuction.discountRate * elapsed;

        //поточна ціна
        return cAuction.startingPrice - discount;
    }

    // Функція купівлі
    function buy(uint index) external payable {
        // Перевірка чи аукціон існує
        require(index < auctions.length, "Auction does not exist!");

        Auction storage cAuction = auctions[index];

        // Перевірки стану аукціону і достатньості коштів
        require(!cAuction.stopped, "stopped!");
        require(block.timestamp < cAuction.endsAt, "ended!");

        uint cPrice = getPriceFor(index);
        require(msg.value >= cPrice, "not enough funds!");

        // Зупинка аукціону
        cAuction.stopped = true;
        cAuction.finalPrice = cPrice;

        // Повернення надлишкової суми коштів покупцю (якщо така є)
        uint refund = msg.value - cPrice;
        if(refund > 0) {
            payable(msg.sender).transfer(refund);
        }

        // Виплата коштів продавцю з урахуванням комісії
        cAuction.seller.transfer(
            cPrice - ((cPrice * FEE) / 100)
        );

        // Виклик події закінчення аукціону
        emit AuctionEnded(index, cPrice, msg.sender);
    }
}