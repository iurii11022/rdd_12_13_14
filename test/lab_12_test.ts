const { expect } = require("chai")
const { ethers } = require("hardhat")

describe("Lab 12", function () {
    let owner
    let other_addr
    let lab12Contract

    beforeEach(async function () {
        [owner, other_addr] = await ethers.getSigners()

        const DemoContract = await ethers.getContractFactory("Lab_12", owner)
        lab12Contract = await DemoContract.deploy()
        await lab12Contract.deployed()
    })

    async function sendMoney(sender) {
        const amount = 100
        const txData = {
            to: lab12Contract.address,
            value: amount
        }

        const tx = await sender.sendTransaction(txData)
        await tx.wait();
        return [tx, amount]
    }

    it("shound allow to send money", async function() {
        const [sendMoneyTx, amount] = await sendMoney(other_addr)

        await expect(() => sendMoneyTx)
            .to.changeEtherBalance(lab12Contract, amount);

        const timestamp = (
            await ethers.provider.getBlock(sendMoneyTx.blockNumber)
        ).timestamp

        await expect(sendMoneyTx)
            .to.emit(lab12Contract, "Paid")
            .withArgs(other_addr.address, amount, timestamp)
    })

    it("shound allow owner to withdraw funds", async function() {
        const [_, amount] = await sendMoney(other_addr)

        const tx = await lab12Contract.withdraw(owner.address)

        await expect(() => tx)
            .to.changeEtherBalances([lab12Contract, owner], [-amount, amount])
    })

    it("shound not allow other accounts to withdraw funds", async function() {
        await sendMoney(other_addr)

        await expect(
            lab12Contract.connect(other_addr).withdraw(other_addr.address)
        ).to.be.revertedWith("you are not an owner!")
    })
})